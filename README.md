![Build Status](https://gitlab.com/pages/gatsby/badges/master/build.svg)

---

Example [Gatsby] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

**Table of Contents**

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml)

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install] Gatsby CLI
1. Generate and preview the website with hot-reloading: `gatsby develop`
1. Add content

Read more at Gatsby's [documentation].

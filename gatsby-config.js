module.exports = {
  pathPrefix: `/gatsby`,
  siteMetadata: {
    title: `Gatsby Test`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
